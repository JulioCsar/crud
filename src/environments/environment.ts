// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA9Q2rjJG0GAf4_SBdaDgc8_UGmR1362Yo",
    authDomain: "crud-jcphdz.firebaseapp.com",
    databaseURL: "https://crud-jcphdz.firebaseio.com",
    projectId: "crud-jcphdz",
    storageBucket: "crud-jcphdz.appspot.com",
    messagingSenderId: "905372078089"
  }
};
