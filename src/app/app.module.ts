import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { TableComponent } from './table/table.component';

import { ModalComponent } from './modal/modal.component';
import { ItemService } from './shared/item.service';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [
    ItemService,
    NgbModal
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }