import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Item } from '../shared/item';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemService {

	private itemDoc: AngularFirestoreDocument<Item>;
  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Item>('faqs');
    this.items = this.itemsCollection.valueChanges();

    this.items = this.itemsCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Item;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });
  }

  getItems() {
    return this.items;
  }

  addItem(item: Item) {
    return this.itemsCollection.add({
      question: item.question,
      answer: item.answer
    });
  }

  updateItem(item: Item) {
    this.itemDoc = this.afs.doc(`faqs/${ item.id }`);
    return this.itemDoc.update(item);
  }

  deleteItem(item: Item) {
    this.itemDoc = this.afs.doc(`faqs/${ item.id }`);
    return this.itemDoc.delete();
  }

}
