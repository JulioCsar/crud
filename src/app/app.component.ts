import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from './modal/modal.component';

import { Item } from './shared/item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';

  @ViewChild(ModalComponent)
  private modal: ModalComponent;

  openModal = (item?: Item) => {
  	this.modal.open(item);
  };

}
