import { Component, ViewChild, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ItemService } from '../shared/item.service';
import { Item } from '../shared/item';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  inputsForm: FormGroup;
  private item: Item = {
  	id: null,
  	question: '',
  	answer: ''
  };
  private modalRef: NgbModalRef;
  private sending: boolean = false;

  @ViewChild('content') private content;

  constructor(private itemService: ItemService, private modalService: NgbModal, private fb: FormBuilder) {
  	this.inputsForm = this.fb.group({
      question: [this.item.question, [Validators.required]],
      answer: [this.item.answer, []]
    });
  }

  ngOnInit() {
  	const inputsControl = this.inputsForm.controls["question"];
    inputsControl.valueChanges.subscribe(value => {
      if (value.trim() === '') {
        inputsControl.setErrors({
          required: true
        });
      }
    });
  }

  open(item: Item = {id: null, question: '', answer: ''}) {
  	this.item = Object.assign({}, item);
    this.modalRef = this.modalService.open(this.content, {
      size: 'lg'
    });
  }

  onSubmit() {
  	let promise;

  	this.item.question = this.item.question.trim();
  	this.item.answer = this.item.answer.trim();
  	this.sending = true;

    if(this.item.id == null){
    	promise = this.itemService.addItem(this.item);
    }
    else{
    	promise = this.itemService.updateItem(this.item);
    }

    promise.then(this.onSuccess).catch(this.onFailure);
  }

  onSuccess = response => {
  	this.modalRef.close();
  	this.sending = false;
  }

  onFailure = error => {
  	this.sending = false;
  	alert('Algo salió mal');
  }

}
