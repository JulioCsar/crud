import { Component, Input, OnInit } from '@angular/core';
import { ItemService } from '../shared/item.service';

import { Item } from '../shared/item';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

	@Input() openModal;
  items: Item[];

  constructor(public itemService: ItemService) { }

  ngOnInit() {
    this.itemService.getItems().subscribe(items => {
      this.items = items;
    });
  }

  deleteItem(event, item) {
    const response = confirm('¿Deseas eliminar el registro?');
    if (response ) {
      this.itemService.deleteItem(item)
      	.then(response => {
      		console.log(response);
      	})
      	.catch(error => {
      		console.log(error);
      		alert('Algo salió mal');
      	});
    }
    return;
  }

  editItem(event, item) {
  	this.openModal(item);
  }
  
}
